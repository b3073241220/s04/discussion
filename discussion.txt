INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 273, "Rock, alternative rock, arena rock", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 8);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 192, "Dancehall-poptropical housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 196, "Pop, R&B", 10);

*Excluding Records
SELECT * FROM songs WHERE id != 1;

*Greater than or equal
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id > 11;

*Get specific ids (OR keyword)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 10;

*Get specific ids (IN keyword)
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

*Combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

*Finding partial matches
==NOTES==
LIKE - Denotes the use of partial matching in searching
	"%a" - Find a pattern in the strings that has "a" at the end
	"%a" - Find a pattern in the strings that has "a" at the start
	"%a" - Find a pattern in the strings that has "a" anywhere in the string
=========
SELECT * FROM songs WHERE song_name LIKE "%a";
SELECT * FROM songs WHERE song_name LIKE "a%";
SELECT * FROM songs WHERE song_name LIKE "%a%";

*Know some keyword
SELECT * FROM albums WHERE date_released LIKE "%201%";

*Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

*Getting distinct records
SELECT DISTINCT genre FROM songs;

--[Section] Table Joins

*Combine atists and albums table
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

*Combine more than two tables (albums, songs, artists)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

*Select columns to be included per table
SELECT artists.name, albums.album_title FROM artists
	JOIN albums ON artists.id = albums.artist_id;

*Show artists without records on the right side of the joined table

SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;